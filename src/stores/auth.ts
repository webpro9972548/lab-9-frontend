import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import authService from '@/services/auth'
import { useMessageStore } from '@/stores/message';
import { useRouter } from 'vue-router';
import { useLoadingStore } from './loading';

export const useAuthStore = defineStore('auth', () => {
  const messageStore = useMessageStore()
  const router = useRouter()
  const loadingStore = useLoadingStore()
  async function login(email: string, password: string) {
    loadingStore.doLoad()
    try{
      const res = await authService.login(email, password)
      console.log(res.data)
      localStorage.setItem('user', JSON.stringify(res.data.user))
      localStorage.setItem('access_token', res.data.access_token)
      router.replace('/')
    } catch(e: any){
      messageStore.showMessage(e.message)
    }
    loadingStore.finish()
  }
  function logOut(){
    localStorage.removeItem('user')
    localStorage.removeItem('access_token')
    router.replace('/login')
  }

  function getCurrentUser(): User | null {
    const user = localStorage.getItem('user')
    if(user === null) return null
    return JSON.parse(user)
  }

  function getToken(): User | null {
    const token = localStorage.getItem('access_token')
    if(token === null) return null
    return JSON.parse(token)
  }

  return { getCurrentUser, login, getToken, logOut}
})
